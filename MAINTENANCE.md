# How to update Gromacs-SWAXS package recipes

`gromacs-*` packages are copies of upstream `gromacs` package with versions/urls changed.

You can see differences with following commands

```
diff --color=always packages/gromacs/package.py custom-packages/packages/gromacs-swaxs/package.py
diff --color=always packages/gromacs/package.py custom-packages/packages/gromacs-swaxs-replica/package.py
```

**Note:** `gromacs` package is updated every few weeks so these `diff` commands will show more differences over time. You might want to put these changes back to our custom packages. If you use VSCode with Git, you can just override our custom `package.py` with upstream `gromacs/package.py`, then revert lines describing modified versions. Just make sure everything is commited in these packages, then VSCode will show you clear versions.
